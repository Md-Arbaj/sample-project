import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Row",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Hello'),
        ),
        body: new Column(
          children: <Widget>[
            new Text('hello1'),
            new Text('hello2'),
            new Text('hello3'),
          ],
        ),
      ),
    );
  }
}
